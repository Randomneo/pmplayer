import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gst
import youtube_dl as yd

def hook(ps):
    print(ps)

"""Youtbue DL options"""
ydl_o = {
    'format': 'bestaudio/best',
    'outtmpl': 'music/%(title)s.%(ext)s',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
    'progress_hooks': [hook],
}
    
def download (url):
    with yd.YoutubeDL(ydl_o) as ydl:
        ydl.download({url})


class download_dialog(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Download Dialog", parent, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_default_size(150, 100)
        self.box = self.get_content_area()

        self.box.set_border_width(6)

        self.label = Gtk.Label("Put download link")
        self.box.add(self.label)

        self.entry = Gtk.Entry()
        self.box.add(self.entry)
        self.show_all()
