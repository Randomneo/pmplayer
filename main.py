from download import download_dialog, download
import os, sys
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gst

class my_window(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Hello World")
        self.set_size_request(400, 200)
        self.vbox = Gtk.VBox(False)
        self.add(self.vbox)

        """
        self.button = Gtk.Button(label="Download")
        self.button.connect("clicked", self.download_click)
        self.vbox.pack_start(self.button, False, False, 0)
        """

        self.add_control_buttons()
        self.add_TreeView()

    def add_control_buttons(self):
        self.play_button = Gtk.Button(label = 'Play')
        self.stop_button = Gtk.Button(label = 'Stop')
        self.download_button = Gtk.Button(label = 'Download')

        self.download_button.connect('clicked', self.download_click)
        self.play_button.connect('clicked', self.on_button_clicked)
        self.stop_button.connect('clicked', self.on_button_clicked)

        self.contorl_box = Gtk.HBox(False)
        self.contorl_box.pack_start(self.play_button, False, False, 5)
        self.contorl_box.pack_start(self.stop_button, False, False, 5)
        self.contorl_box.pack_end(self.download_button, False, False, 5)

        self.vbox.pack_start(self.contorl_box, False, False, 5)


    def add_TreeView(self):
        #create listStore model
        self.music_liststore = Gtk.ListStore(str)
        self.update_treeview(self)

        #TreeView
        self.treeview = Gtk.TreeView.new_with_model(self.music_liststore)
        for i, column_title in enumerate(['Name']):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text = i)
            self.treeview.append_column(column)

        self.scrolablle_treelist = Gtk.ScrolledWindow()
        self.scrolablle_treelist.set_vexpand(True)
        self.vbox.add(self.scrolablle_treelist)
        self.scrolablle_treelist.add(self.treeview)

        self.show_all()

    def on_button_clicked(self, widget):
        print("Hello World")

    def play_click(self, widget):
        pass

    def download_click(self, widget):
        dialog = download_dialog(self)
        res = dialog.run()

        if res == Gtk.ResponseType.OK:
            uri = dialog.entry.get_text()
            dialog.label.set_text("Downloading")
            dialog.show_all()
            try:
                download(uri)
                self.info_message("You uri downloaded in to music dir.")
                self.update_treeview(self)
            except:
                self.error_message("Cant download uri.")
        else:
            print("Cancel")

        dialog.destroy()

    def error_message(self, error_message):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                                   Gtk.ButtonsType.OK, "Error")
        dialog.format_secondary_text(error_message)
        dialog.run()
        dialog.destroy()

    def info_message(self, info_message):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
                                   Gtk.ButtonsType.OK, "Info")
        dialog.format_secondary_text(info_message)
        dialog.run()
        dialog.destroy()

    def update_treeview(self, widget):
        self.music_liststore.clear()
        for item in os.listdir('./music/'):
            self.music_liststore.append([item, ])

win = my_window()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
